Aplikace spustitelná pod linux-x64 je v složce build. Zde jsou umístěny také korpusy.

Aplikace se používá
```
./ngram <korpus> <n>
```

Výstupní CSV jsou v kořenové složce projektu. Jedná se o výstupy SENCZ_n-gram.csv a SENEN_n-gram.csv.
V těchto souborech je perplexita na prvním řádku.

SRILM výstup je v složce SRILM.
\*.count jsou počty n-gramů
\*.lm jazykové modely
\*.ppl perplexita

Výsledné soubory po těchto příkazech:
```
ngram-count -text SENCZ.txt -order 2 -write czbigram.count -unk
ngram-count -read czbigram.count -order 2 -lm czbigram.lm
ngram -ppl SENCZ.txt -order 2 -lm czbigram.lm >> czbigram.ppl
```
